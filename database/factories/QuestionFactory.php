<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'question'=>$faker->sentence(rand(5,6)),
        'marks'=>rand(1,4),
        'difficulty_level'=>rand(1,4)
    ];
});
