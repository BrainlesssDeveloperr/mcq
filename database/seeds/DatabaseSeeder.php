<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Student;
use App\Teacher;
use App\Subject;
use App\Answer;
use App\Chapter;
use App\Question;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class,15)->create();
        $subjects = fopen('C:\subjects.csv', 'r');

        $s_id = 0;
        $i=0;
        $c_id = 0;
        while (($line = fgetcsv($subjects)) !== FALSE) {
            $subject = \App\Subject::create([
                'name'=>$line[0]
            ]);
            $s_id = $subject->id;
        }
        $chapters = fopen('C:\chapters.csv', 'r');
        while (($chap = fgetcsv($chapters)) !== FALSE) {
            $chapter = \App\Chapter::create([
                'subject_id'=>rand(1,$s_id),
                'name'=>$chap[0]
            ]);
            $c_id = $chapter->id;
        }
        $questions = fopen('C:\questions.csv', 'r');
        while (($ques = fgetcsv($questions)) !== FALSE) {
            $question = \App\Question::create([
                'question'=>utf8_encode($ques[0]),
                'chapter_id'=>rand(1,5),
                'marks'=>rand(1,2),
                'difficulty_level'=>rand(1,4),
            ]);
        }
        $answers = fopen('C:\options.csv', 'r');
        while (($ans = fgetcsv($answers)) !== FALSE) {
            $answer = \App\Answer::create([
                'answer'=>$ans[0],
                'question_id'=>$ans[1],
                'created_at'=>now(),
                'updated_at'=>now()
            ]);
        }

        fclose($subjects);

        fclose($chapters);
    }

}
