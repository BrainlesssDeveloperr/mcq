<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerQuestionTestUser extends Model
{
    protected $table = 'answer_question_test_user';
    protected $guarded = [];
}
