<?php

namespace App\Http\Middleware;

use Closure;

class IsTestGiven
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $test = $request->route()->parameters()['test'];
        $isTestGiven = $test->users()->where('user_id',auth()->user()->id)->first();
        if($isTestGiven!=null){
            return redirect(abort(401));
        }
        return $next($request);
    }
}
