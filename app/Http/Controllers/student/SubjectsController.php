<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use App\Subject;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $subjects = Subject::paginate(8);
        $arr = [];
        foreach($subjects as $subject){
            $subarr = [];
            $chapters = $subject->chapters()->where('subject_id',$subject->id)->get();
            $final_test = null;
            // dd(Carbon::now()->format('Y-m-d H:i:s'));
            $tests = $subject->tests()->where('test_type','schedule')->orderBy('scheduled_at','ASC')->whereDate('scheduled_at','>=',Carbon::now()->format('Y-m-d'))->get();
            // dd($tests);
            foreach($tests as $test){
                // dd($test);
                $testTime= Carbon::parse($test->scheduled_at);
                $startTime = $testTime->timestamp;
                $endTime = date("Y-m-d H:i:s",(($test->duration/1000)+$startTime));
                $isTestGiven = $test->users()->where('user_id',auth()->user()->id)->first();
                if($endTime >= Carbon::parse(now()->format('Y-m-d H:i:s')) && $isTestGiven==null)
                {
                    $final_test= $test;
                    break;
                }
            }
            $testTime = null;
            $startTime = null;
            $endTime = null;
            $now = null;
            if($final_test!=null){
                $testTime= Carbon::parse($final_test->scheduled_at);
                $startTime = $testTime->timestamp;
                $endTime = date("Y-m-d H:i:s",(($final_test->duration/1000)+$startTime));
                $now = Carbon::now();
            }
            $subarr[] = $subject;
            $subarr[] = $chapters;
            $subarr[] = $testTime;
            $subarr[] = $now;
            $subarr[] = $endTime;
            $subarr[] = $final_test;
            $arr[] = $subarr;
        }

        // dd($tests);
        return view('student.subjects.index',compact([
            'subjects','arr'
        ]));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        //
        $chapters = $subject->chapters()->where('subject_id',$subject->id)->get();
        $testChapter = $subject->tests()->where('subject_id',$subject->id)->get();
        // dd($testChapter);
        $final_test = null;
        // dd(Carbon::now()->format('Y-m-d H:i:s'));
        $tests = $subject->tests()->where('test_type','schedule')->orderBy('scheduled_at','ASC')->whereDate('scheduled_at','>=',Carbon::now()->format('Y-m-d'))->get();
        // dd($tests);
        foreach($tests as $test){
            // dd($test);
            $testTime= Carbon::parse($test->scheduled_at);
            $startTime = $testTime->timestamp;
            $endTime = date("Y-m-d H:i:s",(($test->duration/1000)+$startTime));
            $isTestGiven = $test->users()->where('user_id',auth()->user()->id)->first();
            if($endTime >= Carbon::parse(now()->format('Y-m-d H:i:s')) && $isTestGiven==null)
            {
                $final_test= $test;
                break;
            }
        }
        $testTime = null;
        $startTime = null;
        $endTime = null;
        $now = null;
        if($final_test!=null){
            $testTime= Carbon::parse($final_test->scheduled_at);
            $startTime = $testTime->timestamp;
            $endTime = date("Y-m-d H:i:s",(($final_test->duration/1000)+$startTime));
            $now = Carbon::now();
        }
        return view('student.chapters.show',compact([
            'subject','chapters','testTime','now','endTime','final_test','testChapter'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subject $subject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        //
    }
}
