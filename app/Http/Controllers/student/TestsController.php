<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use App\Question;
use App\Test;
use App\Subject;
use App\Answer;
use App\Http\Requests\student\Tests\CreateTestRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Carbon\Carbon;

class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Subject $subject)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Subject $subject)
    {
        return view('student.quiz.create',compact([
            'subject'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTestRequest $request,Subject $subject)
    {
        // dd($request->all());
        $total = 0;
        $duration = 0;
        foreach($request->mainWeightage as $chpWeightage){
            if($chpWeightage!=null){
                $cw = explode(':',$chpWeightage);
                $total += $cw[1];

            }
        }
        if($total<=25){
            $duration = 60 * 60 * 1000;
        }elseif($total >= 26 && $total <=60){
            $duration = 60 * 60 * 2 * 1000;
        }elseif($total >= 51){
            $duration = 60 * 60 * 3 * 1000;
        }

        $test = Test::create([
            'test_type'=>'practice',
            'duration'=>$duration,
            'total_marks'=>$total
        ]);
        foreach($request->mainWeightage as $chpWeightage){
            if($chpWeightage!=null){
                $cw = explode(':',$chpWeightage);
                $test->chapters()->attach($cw[0],['weightage'=>$cw[1]]);
                session()->put($cw[0],$cw[1]);
            }
        }
        $test->subjects()->attach($subject->id);


        return view('student.quiz.index',compact([
            'test','subject'
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test,Subject $subject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test,Subject $subject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test,Subject $subject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test,Subject $subject)
    {
        //
    }
    public function ready(Subject $subject,Test $test)
    {
        // s($test);
        return view('student.quiz.index',compact([
            'test','subject'
        ]));

    }
    public function generateQuestion(Test $test,Subject $subject,$active){
        /**
         * Seession
         * 1. whichNo => QuestionId:w/r:ans1:ans (done)
         * 2. whichNoArry => whichNo1:whichNo2: (done)
         * 3. diff => tough (done)
         *random chapter -> check for weightage if less bring unique question check session (difficulytLvl) . save answer kia toh agar right hai toh difficulty badaya aur wrong hai toh kam karde . update marks in session and check kar
         */
        $chapters = $test->chapters;
        $chp_count = $test->chapters->count();
        if($active!=-1 && str_contains(session()->get($active),':')){
            $question = Question::find(explode(':',session()->get($active))[0]);
            return $question;
        }
        $takeQuestion=0;
        $take=0;
        if(session()->has('question_list')){
            $takeQuestion = count($this->getQuestionList());
        }
        else
            $take = 1;
        while($takeQuestion!=session()->get('totalQuestions') || $take==1){
            $chp_index = rand(0,$chp_count-1);
            $chapter = $chapters[$chp_index];
            if($this->isAvailable($chapter)!=0){
                $question_list = $this->getQuestionList();

                $q_list = [];
                if(session()->get('oneMarksQuestions')!=0){
                    $markCondition = ['marks','=',1];
                    session()->put('oneMarksQuestions',((int)session()->get('oneMarksQuestions'))-1);
                }else{
                    $markCondition = ['marks','=',2];
                    session()->put('twoMarksQuestions',((int)session()->get('twoMarksQuestions'))-1);
                }
                if($question_list==null){
                    $question = $chapter->questions()->where([
                        ['difficulty_level','=',session()->get('difficulty')],
                        $markCondition
                    ])->get()->random(1)[0];
                    $test->questions()->attach($question->id);
                }else{

                    foreach($question_list as $q){
                        $q_list[] = ['id','!=',$q];
                    }
                    $q_list[] = ['difficulty_level','=',session()->get('difficulty')];
                    $q_list[] = $markCondition;
                    // dd($chapter->questions()->where($q_list)->get());
                    $question = $chapter->questions()->where($q_list)->get()->random(1)[0];
                    // dd($question);

                    $test->questions()->attach($question->id);
                }
                $this->updateChapterSession($chapter,$question);
                $this->setQuestionSession($question,$test,$active);
                $take=0;
                return $question;
            }
        }
        return null;
    }

    public function isAvailable($chapter){
        $arr = explode(':',session()->get('chp'.$chapter->id));
        return (((int)$arr[0])-((int)$arr[1]));
    }
    public function updateChapterSession($chapter,$question){
        $last = (int)(explode(':',session()->get('chp'.$chapter->id))[1]);
        session()->put('chp'.$chapter->id,$chapter->pivot->weightage.':'.($last+$question->marks));
    }
    public function setChaptersSession($test){
        foreach($test->chapters as $chapter){
            session()->put('chp'.$chapter->id,$chapter->pivot->weightage.':0');
        }
    }
    public function setQuestionSession($question,$test,$active){
        if(!session()->has('question_list'))
            session()->put('question_list',$question->id);
        else
            session()->put('question_list',session()->get('question_list').':'.$question->id);
        if($active>=0)
            session()->put($active,$question->id.":0:0");

    }
    public function getQuestionList(){
        if(!session()->has('question_list'))
            return null;
        else
            return explode(':',session()->get('question_list'));
    }
    public function getUnmarkedQuestionFromSession(){
        $arr = explode(':',session()->get('whichNoArray'));
        $no = 1;
        if(session()->has('nextUmarked')){
            $no = session()->get('nextUmarked');
        }
        for($i = $no;$i<=session()->get('totalQuestions');$i++){
            if(!in_array($i,$arr)){
                session()->put('nextUmarked',$i+1);
                return $i;
            }
        }
        return -1;

    }
    public function setSessions(Test $test){
        $totalMarks = $test->total_marks;
        $totalQuestions = (int) floor($totalMarks - (($totalMarks * 30)/100));
        session()->put('totalMarks',$totalMarks);
        session()->put('twoMarksQuestions',((int)ceil(($totalMarks * 30)/100)));
        session()->put('oneMarksQuestions',($totalMarks - (((int)session()->get('twoMarksQuestions'))*2)));
        // dd(session()->get('oneMarksQuestions'));
        session()->put('totalQuestions',session()->get('twoMarksQuestions')+session()->get('oneMarksQuestions'));
        session()->put('totalQuestionPage',(int) ceil(session()->get('totalQuestions')/10));
    }
    public function setIndexSession(){
        for($i = 0;$i<=session()->get('totalQuestions');$i++)
            session()->put($i,'');
    }
    public function startTest(Subject $subject,Test $test)
    {
        // dd(session()->all());

        session()->put('time',$test->duration);
        if(!(session()->has('countdown') && session()->has('current_ts') && session()->has('difficulty'))){
            $this->setChaptersSession($test);
            session()->put('countdown',$test->duration);

            if($test->test_type == 'schedule'){
                session()->put('current_ts',Carbon::parse($test->scheduled_at)->timestamp);
            }
            else
                session()->put('current_ts',now()->timestamp);

            session()->put('difficulty',1);
            $this->setSessions($test);
            session()->put('whichNoArray','0:0');
            session()->put('rightAnsCount',0);
            $this->setIndexSession();
        }
        $str = session()->get(1);
        $ans = null;
        if(str_contains($str,':')){
            $question = Question::find(explode(':',$str)[0]);
            $ans = Answer::find(explode(':',$str)[2]);
        }
        else
            $question = $this->generateQuestion($test,$subject,1);
        return view('student.quiz.single-question',compact(['subject','test','question','ans']));
    }
    public function getUnmarkedQuestion(Request $request){
        if($request->n==-1 && session()->has('nextUmarked'))
            session()->forget('nextUmarked');

        $unmarkedNo = $this->getUnmarkedQuestionFromSession();
        if($unmarkedNo==-1){
            $arr = ['done'];
            return json_encode($arr);
        }else{
            if(session()->has($unmarkedNo) && str_contains(session()->get($unmarkedNo),':')){

                $arr = explode(':',session()->get($unmarkedNo));
                $question_id = $arr[0];
                $correct_ans_id = $arr[2];
                $question = Question::find($question_id);
                $answers = $question->answers;
                $arr = [];
                $whichNoArry = session()->get('whichNoArray');
                $arr[] = $question;
                $arr[] = $answers;
                $arr[] = $unmarkedNo;
                $arr[] = $whichNoArry;
                $arr[] = $correct_ans_id;
                return json_encode($arr);
            }else{
                $question = $this->generateQuestion(Test::find($request->testID),Subject::find($request->subjectID),$unmarkedNo);
                $answers = $question->answers;
                $arr = [];
                $whichNoArry = session()->get('whichNoArray');
                $arr[] = $question;
                $arr[] = $answers;
                $arr[] = $unmarkedNo;
                $arr[] = $whichNoArry;
                $arr[] = null;
                return json_encode($arr);
            }
        }
    }
    public function processQuestion(Request $request){
        $question =  Question::find($request->question_id);
        $correct_ans = $question->correct_answers;
        $difficulty = $question->difficulty_level;

        foreach($correct_ans as $ans){

            if($ans->id == $request->answer_id){
                session()->put($request->active,$request->question_id.":1:".$request->answer_id);
                if(((int)session()->get('rightAnsCount'))>=1){
                    session()->put('rightAnsCount',0);
                    if(((int)session()->get('difficulty'))<4){
                        session()->put('difficulty',$difficulty+1);
                    }
                }
                else
                    session()->put('rightAnsCount',1);
            }else{
                session()->put($request->active,$request->question_id.":0:".$request->answer_id);
                session()->put('rightAnsCount',0);
                if($difficulty>1)
                    session()->put('difficulty',$difficulty-1);
            }
        }

        if(session()->has('whichNoArray') && $request->answer_id!=null){
            if(str_contains(session()->get('whichNoArray'),':')){
                $activeArray =  explode(':',session()->get('whichNoArray'));
                if(!in_array($request->active,$activeArray)){
                    session()->put('whichNoArray',session()->get('whichNoArray').":".$request->active);
                }
            }
        }
        if(session()->has('whichNoArray') && $request->answer_id==null){
            $activeArray =  explode(':',session()->get('whichNoArray'));
            $str = '0:0';
            foreach($activeArray as $item){
                if($item!=$request->active && $item!=0){
                    $str = $str.':'.$item;
                }
            }
            session()->put('whichNoArray',$str);

        }
        if($request->active != ((int)session()->get('totalQuestions'))){
            if($request->prev == 1){
                $active = $request->active - 1;
            }else{
                $active = $request->active + 1;
            }
        }else{
            $active = $request->active;
        }
        if(session()->has($request->active) && str_contains(session()->get($request->active),':')){
            $question = $this->generateQuestion(Test::find($request->testID),Subject::find($request->subjectID),$active);

            if(str_contains(session()->get($active),':')){
                $arr = explode(':',session()->get($active));
                $correct_ans_id = $arr[2];
            }else{
                $correct_ans_id = null;
            }
            // Session::put('active',$active);
            $arr = [];
            $whichNoArry = session()->get('whichNoArray');
            $answers = $question->answers;
            $arr[] = $question;
            $arr[] = $answers;
            $arr[] = $active;
            $arr[] = $whichNoArry;
            $arr[] = $correct_ans_id;

            return json_encode($arr);
        }
    }
    public function navigateQuestion(Request $request){
        if(session()->has($request->active) && str_contains(session()->get($request->active),':')){

            $arr = explode(':',session()->get($request->active));
            $question_id = $arr[0];
            $correct_ans_id = $arr[2];
            $question = Question::find($question_id);
            $answers = $question->answers;
            $arr = [];
            $whichNoArry = session()->get('whichNoArray');
            $arr[] = $question;
            $arr[] = $answers;
            $arr[] = $request->active;
            $arr[] = $whichNoArry;
            $arr[] = $correct_ans_id;
            return json_encode($arr);
        }else{
            $question = $this->generateQuestion(Test::find($request->testID),Subject::find($request->subjectID),$request->active);
            $answers = $question->answers;
            $arr = [];
            $whichNoArry = session()->get('whichNoArray');
            $arr[] = $question;
            $arr[] = $answers;
            $arr[] = $request->active;
            $arr[] = $whichNoArry;
            $arr[] = null;
            return json_encode($arr);
        }
    }
    public function navigateFilledQuestion(Request $request){
        $whichNoArry = session()->get('whichNoArray');
        return json_encode($whichNoArry);
    }
}
