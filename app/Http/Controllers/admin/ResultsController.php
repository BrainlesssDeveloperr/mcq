<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Test;
use App\User;
class ResultsController extends Controller
{
    public function getResult(Test $test){

        $test_users = $test->users;
        $arr = [];
        foreach($test_users as $test_user){
            $arr[] = ['id','!=',$test_user->id];
        }
        $arr[] = ['role','!=','admin'];
        $users = User::where($arr)->get();
        $marksArr = [];
        $totalMarks = 0;
        foreach($test_users as $item){
            $marksArr[] = $item->pivot->marks;
            $totalMarks+=$item->pivot->marks;
        }
        $min = (min($marksArr));
        $max = (max($marksArr));
        $avg = $totalMarks/$test->users->count();
        return view('admin.tests.result',compact([
            'min','max','avg','test_users','users'
        ]));
    }
}
