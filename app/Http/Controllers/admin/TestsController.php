<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tests\CreateTestRequest;
use App\Notifications\SendTestLink;
use App\Rules\IsSlotAvailable;
use App\Subject;
use App\Test;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestsController extends Controller
{
    public function index(Subject $subject)
    {
        $tests = $subject->tests;
        return view('admin.tests.index',compact([
            'subject','tests'
        ]));
    }

    public function create(Subject $subject)
    {
        return view('admin.tests.create',compact([
            'subject'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postCustomValidation(Request $request){
        $this->validate($request, [
            'schedule_date' => 'is_available',
        ]);
        return true;
    }

    public function store(CreateTestRequest $request,Subject $subject)
    {
        // dd($request);
        $this->postCustomValidation($request);
        //  dd($this->validate($request, ['schedule_date' => new IsSlotAvailable]));
        if($this->validate($request, ['schedule_date' => new IsSlotAvailable])){

        }
        $total = 0;
        $duration = 0;
        foreach($request->mainWeightage as $chpWeightage){
            if($chpWeightage!=null){
                $cw = explode(':',$chpWeightage);
                $total += $cw[1];

            }
        }
        if($total<=25){
            $duration = 60 * 60 * 1000;
        }elseif($total >= 26 && $total <=60){
            $duration = 60 * 60 * 2 * 1000;
        }elseif($total >= 51){
            $duration = 60 * 60 * 3 * 1000;
        }


        $test = Test::create([
            'test_type'=>'schedule',
            'scheduled_at'=>$request->schedule_date,
            'duration'=>$duration,
            'total_marks'=>$total
        ]);
        foreach($request->mainWeightage as $chpWeightage){
            if($chpWeightage!=null){
                $cw = explode(':',$chpWeightage);
                $test->chapters()->attach($cw[0],['weightage'=>$cw[1]]);
                session()->put($cw[0],$cw[1]);
            }
        }
        $test->subjects()->attach($subject->id);

        $users = User::where('role','student')->get();
        $test = $subject->tests()->where('test_type','schedule')->get();
        $testTime= Carbon::parse($test[0]->scheduled_at);
        $startTime = $testTime->timestamp;
        $now = Carbon::now()->timestamp;
        $when = $now - $startTime;
        $test = Test::orderBy('id','DESC')->first();
        // foreach($users as $user){
        //      $user->notify(new SendTestLink($subject,$test));
        // }
        return redirect(route('subjects.tests.index',$subject));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject,Test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
}
