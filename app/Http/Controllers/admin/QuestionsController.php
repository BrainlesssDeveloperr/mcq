<?php

namespace App\Http\Controllers\admin;

use App\Answer;
use App\Chapter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Questions\CreateQuestionRequest;
use App\Http\Requests\Admin\Questions\UpdateQuestionRequest;
use App\Question;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chapter $chapter)
    {
        $questions = $chapter->questions()->withoutTrashed()->get();
        return view('admin.questions.index',compact([
            'questions','chapter'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin\questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request)
    {
        //
        // dd($request->answers);
        $chapter = Chapter::find($request->chapter_id);
        // dd($chapter);
        $data = [
            'question'=>$request->question,
            'marks'=>$request->marks,
            'difficulty_level'=>$request->difficulty_lvl
        ];
        $question= $chapter->questions()->create($data);

        foreach($request->answers as $answer){
            $answerData = [
                 'question_id' => $question->id,
                'answer' =>$answer
            ];
            $answer = Answer::create($answerData);
        }
        $answers = Answer::where('question_id',$question->id)->get();
        foreach($request->checkbox as $checkbox){
            foreach($answers as $answer){
                if($answer->answer == $checkbox){
                    $question->correct_answers()->attach($answer->id);
                }
            }
        }
        $questions = $chapter->questions()->withoutTrashed()->get();
        return view('admin.questions.index',compact([
            'questions','chapter'
        ]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request,Chapter $chapter, Question $question)
    {
        $data = [
            'question'=>$request->question,
            'marks'=>$request->marks,
            'difficulty_level'=>$request->difficulty_lvl
        ];
        $questionUpdate = $question->update($data);


        $i=0;
        foreach($question->answers as $ans){
            $ans->update([
                'answer'=>$request->answers[$i]
            ]);
            $i++;
        }

        $answers = Answer::get()->where('question_id',$question->id);
        $arr = [];
        foreach($request->checkbox as $checkbox){
            foreach($answers as $answer){
                if($answer->answer == $checkbox){
                    $arr[] = $answer->id;
                }
            }
        }
        $question->correct_answers()->sync($arr);
        $questions = $chapter->questions;
        return view('admin.questions.index',compact([
            'questions','chapter'
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter,Question $question)
    {
        $question->delete();
        $questions = $chapter->questions;
        return view('admin.questions.index',compact([
            'questions','chapter'
        ]));
    }
    public function createQuestion($chapter){
        return view('admin.questions.create',compact([
            'chapter'
        ]));
    }
    public function editQuestion($chapter_id,Question $question){
        $chapter = Chapter::find($chapter_id);
        return view('admin.questions.edit',compact([
            'chapter','question'
        ]));
    }
}
