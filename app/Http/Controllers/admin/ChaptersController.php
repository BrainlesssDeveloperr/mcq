<?php

namespace App\Http\Controllers\admin;

use App\Chapter;
use App\Subject;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChaptersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Subject $subject)
    {
        //yeh sab subjects laaega
        // $chapters = Chapter::withoutTrashed()->get();
        // return view('admin.chapters.index',compact(
        //     'chapters'
        // ));
        //yeh subject ka chapters laaega
        $chapters = $subject->chapters()->withoutTrashed()->get();
        return view('admin.chapters.index',compact([
            'subject','chapters'
        ]));
    }
    public function create(Subject $subject)
    {
        return view('admin.chapters.create',compact([
            'subject'
        ]));
    }
    public function show()
    {
       //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Subject $subject)
    {
        $data = $request->all();
        foreach($data['name'] as $chapter){
            $subject->chapters()->create([
                'name' => $chapter
            ]);
        }
        return redirect(route('subject.chapters.index',$subject));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function editChapter(Chapter $chapter)
    {
        return view('admin.chapters.edit',compact([
            'chapter'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function updateChapter(Request $request, Chapter $chapter)
    {
        $data = $request->all();
        $chapter->update([
            'name'=> $data['name']
        ]);
        $subject = $chapter->subject;
        $chapters = $subject->chapters()->withoutTrashed()->get();
        return view('admin.chapters.index',compact([
            'subject','chapters'
        ]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroyChapter(Chapter $chapter)
    {
        if(count($chapter->questions)==0){
            $chapter->delete();
        }
        return redirect()->back();
    }

    public function createChapter()
    {
        //yeh function hai jab chapters se create kia aur subject id nai hai toh sab subjects bhejra hu dropdown ke liye
       $subjects = Subject::all();
       $subject = null;
        // dd($subject==-1);
        return view('admin.chapters.create',compact([
            'subject','subjects'
        ]));
    }
    public function indexChapter()
    {
        $subject = null;
        // yeh sab subjects laaega
        $chapters = Chapter::withoutTrashed()->get();
        return view('admin.chapters.index',compact(
            'chapters','subject'
        ));
    }
}
