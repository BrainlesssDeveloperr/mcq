<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Test;
use App\Answer;
use App\AnswerQuestionTestUser;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class ResultsController extends Controller
{
    public function calculateResult(Test $test){

        $marks = 0;
        $indexArr = explode(':',session()->get('whichNoArray'));
        $isTestGiven = $test->users()->where('user_id',auth()->user()->id)->first();

        if($isTestGiven==null){
            foreach($indexArr as $index){
                if($index!=0){
                    $single = session()->get($index);

                    if(str_contains($single,':'))
                    {
                        $singleArr = explode(':',$single);
                        $question = Question::find($singleArr[0]);

                        if($singleArr[1]==1){
                            $marks += $question->marks;
                        }

                        $aqtu = AnswerQuestionTestUser::create([
                            'answer_id'=>$singleArr[2],
                            'question_id'=>$singleArr[0],
                            'test_id'=>$test->id,
                            'user_id'=>auth()->user()->id
                        ]);
                    }
                }
            }


            $test->users()->attach(auth()->user()->id,['marks'=>$marks]);
            $this->unsetOtherSessions();
        }
        $totalMarks = $test->where('id',$test->id)->get('total_marks');
        $totMarks = $totalMarks[0]->total_marks;
        $questions = AnswerQuestionTestUser::where([
            ['user_id',Auth::id()],
            ['test_id',$test->id]
        ])->get();

        $final_ans = new Collection();
        if(!($test->test_type == 'schedule'))
        {
            foreach($questions as $item){
                $question = Question::find($item->question_id);
                $ans = Answer::find($item->answer_id);
                $correct_ans = $question->correct_answers;
                $arr = [$question ,$ans ,$correct_ans[0]];
                $final_ans->push($arr);
            }
        }

        $isTestGiven = $test->users()->where('user_id',auth()->user()->id)->first();
        $marks = $isTestGiven->pivot->marks;


        return view('student.quiz.result',compact([
            'totMarks','final_ans','test','marks'
        ]));
    }
    public function unsetOtherSessions(){
        session()->forget('whichNoArray');
        for($i = 1;$i<=session()->get('totalQuestions');$i++)
            session()->forget($i);
        session()->forget('totalQuestions');
        session()->forget('totalMarks');
        session()->forget('twoMarksQuestions');
        session()->forget('oneMarksQuestions');
        session()->forget('totalQuestionPage');
        session()->forget('time');
        session()->forget('countdown');
        session()->forget('current_ts');
        session()->forget('difficulty');
        session()->forget('question_list');
    }
}
