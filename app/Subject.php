<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Chapter;
use Illuminate\Database\Eloquent\SoftDeletes;
class Subject extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function chapters(){
        return $this->hasMany(Chapter::class);
    }
    public function tests(){
        return $this->belongsToMany(Test::class);
    }
}
