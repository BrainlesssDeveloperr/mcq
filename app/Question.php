<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Question extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function chapter(){
        return $this->belongsTo(Chapter::class);
    }
    public function correct_answers(){
        return $this->belongsToMany(Answer::class);
    }
    public function answers(){
        return $this->hasMany(Answer::class);
    }
    public function tests(){
        return $this->belongsToMany(Test::class);
    }
}
