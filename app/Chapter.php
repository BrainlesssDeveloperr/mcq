<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Chapter extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function subject(){
        return $this->belongsTo(Subject::class);
    }
    public function questions(){
        return $this->hasMany(Question::class);
    }
    public function tests(){
        return $this->belongsToMany(Test::class)->withPivot('weightage');
    }
}
