<?php

namespace App\Providers;

use App\Test;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('is_available', function($attribute, $value, $parameters) {
            // dd($parameters);
            return Test::isSlotAvailable($value);
        });
    }
}
