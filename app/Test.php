<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    public function chapters(){
        return $this->belongsToMany(Chapter::class)->withPivot('weightage');
    }
    public function subjects(){
        return $this->belongsToMany(Subject::class);
    }
    public function questions(){
        return $this->belongsToMany(Question::class);
    }
    public function users(){
        return $this->belongsToMany(User::class)->withPivot('marks');
    }
    public static function isSlotAvailable($scheduleAt){
        $tests = Test::whereDate('scheduled_at','<=',$scheduleAt)->get(); //iska end time apna schedule ke beechmei hai ke nai
        // kiska starttime apna end time ke beechmei hai kyaaa
        $testsFuture = Test::whereDate('scheduled_at','>',$scheduleAt)->get();

        $testTime= Carbon::parse($scheduleAt);
        $startTime = $testTime->timestamp;
        $endTime = date("Y-m-d H:i:s",((12600)+$startTime));
        foreach($tests as $test){
            $testEndTime = date("Y-m-d H:i:s",((Carbon::parse($test->scheduled_at)->timestamp) + (($test->duration)/1000)));
            if($testEndTime>$scheduleAt && $testEndTime<$endTime){
                return false;
            }
        }
        foreach($testsFuture as $test){
            $testStartTime = $test->scheduled_at;
            if($testStartTime>$scheduleAt && $testStartTime<$endTime){
                return false;
            }
        }
        return true;
    }
}
