@extends('layouts.dashboard')

@section('content')
    <section class="quiz-view">
        <div class="container">

            <div class="row">

                <div class="col-sm-12 col-md-12">
                    <div class="quiz-result">
                        <h3>Stats</h3>
                        <div class="result-info">
                            <div class="info-slide">
                                <p>Average <span>{{$avg}}</span></p>
                            </div>
                            <div class="info-slide">
                                <p>Max <span>{{$max}}</span></p>
                            </div>
                            <div class="info-slide">
                                <p>Min <span>{{$min}}</span></p>
                            </div>
                        </div>
                        <div class="leaderboard">
                            <h3>leaderboard</h3>

                            @foreach ($test_users as $test_user)
                                <div class="qustion-review">
                                    <p>{{$test_user->name}}<span> {{$test_user->pivot->marks}}</span></p>
                                </div>
                            @endforeach
                            @foreach($users as $user)
                                <div class="qustion-review">
                                    <p>{{$user->name}}<span> Not Attempted</span></p>
                                </div>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('styles')
<style>
/*=-=-=-=-=-=-=-=-=-=-=- Quiz Result Start -=-=-=-=-=-=-=-=-=-=-=*/
.quiz-result {
    padding: 20px;
    margin: 0 0 0 30px;
    background: #fff;
}

.quiz-result h3 {
    position: relative;
    padding-bottom: 10px;
    display: inline-block;
    font-family: "Roboto", sans-serif;
}

.quiz-result h3:after {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 30px;
    height: 2px;
    background: #307ad5;
    content: "";
}

.quiz-result .result-info {
    display: block;
    width: 100%;
    padding: 30px 0 0 0;
}

.quiz-result .result-info .info-slide {
    display: block;
    width: 100%;
    padding-bottom: 10px;
}

.quiz-result .result-info .info-slide p {
    display: block;
    margin: 0;
    font-size: 18px;
    color: #666;
    line-height: 30px;
}

.quiz-result .result-info .info-slide p span {
    float: right;
}

.quiz-result .leaderboard {
    padding: 30px 0 0 0;
}

.quiz-result .leaderboard h3 {
    margin-bottom: 30px
}

.quiz-result .qustion-review {
    display: block;
    padding-bottom: 10px;
}

.quiz-result .qustion-review p {
    display: block;
    margin: 0;
    font-size: 15px;
    color: #666;
    line-height: 24px;
}

.quiz-result .qustion-review p span {
    float: right;
}
.outline{
    border: solid 1px #CCC;
}

.result{
    padding: 0 10px 0 40px;
}
.correct{
    color: #32CD32;
    font-size: 18px;
}
.wrong{
    color: red;
    font-size: 18px;
}
.myAnswer{
    font-size: 12px;
}
/*=-=-=-=-=-=-=-=-=-=-=- Quiz Result End -=-=-=-=-=-=-=-=-=-=-=*/
</style>
@endsection



