
@extends('layouts.dashboard')

@section('content')
    <div class="wrapper d-flex flex-column mt-4">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card shadow mb-4">
                <div class="card-header">
                  <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-plus"></i>Add Chapters
                  </h6>
                </div>
                <!--CARD BODY-->
                <div class="card-body">
                    <form action="{{$subject!=null?route('subject.chapters.store',[$subject->id]):''}}" method="POST" id="add-category">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                @if ($subject==null)

                                    <div class="form-group" id="subject">
                                        <div>
                                            <label>Subject</label>
                                            <select name="subject_id" id="subject_id"  class="custom-select form-control">
                                                    <option value="" selected disabled>Select</option>
                                                @foreach ($subjects as $subject)
                                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group" id="chapters">
                                    <div class="d-flex justify-content-between mb-2">
                                        <label>Chapter Name</label>
                                        <div class="btn btn-outline-primary mb-3" id="add-chapter" onclick="myFunction()">Add More Chapter</div>
                                    </div>
                                    <input type="text" class="form-control" name="name[]" id="name" placeholder="Enter Chapter name" value = "">
                                </div>
                            </div>
                        </div>
                        <input type="submit" name="add_category" class="btn btn-outline-success" value="Submit">
                    </form>
                </div>
                <!--/CARD BODY-->
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/ui/1.8.0/jquery-ui.js" integrity="sha256-d/98Bs8aAQdn5s3reV2fU8guAodmF4UhPjEt/pVkiJo=" crossorigin="anonymous"></script>
    <script>
        function myFunction(){
            var chapter = document.getElementById('chapters');
            $('#chapters').append(`<input type="text" class="form-control mt-3" name="name[]" id="name" placeholder="Enter Chapter name" value = "">`);
        }


        $('#subject_id').change(function() {
            val = $(this).val();
            var url = '{{ route("subject.chapters.store", ":id") }}';
            url = url.replace(':id', val);
            $("#add-category").attr('action',url);
        });
    </script>
@endsection
