
@extends('layouts.dashboard')

@section('content')
    <div class="wrapper d-flex flex-column mt-4">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card shadow mb-4">
                <div class="card-header">
                  <h6 class="m-0 font-weight-bold text-primary">
                    <i class="fa fa-plus"></i>Add Chapters
                  </h6>
                </div>
                <!--CARD BODY-->
                <div class="card-body">
                  <form action="{{ route('chapter.updateChapter',$chapter->id) }}" method="POST" id="add-category">
                    @csrf
                    @method('PUT')
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group" id="chapters">
                            <div class="d-flex justify-content-between mb-2">
                                <label>Chapter Name</label>
                            </div>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Enter Category name" value = "{{$chapter->name}}">
                        </div>
                      </div>
                    </div>
                    <input type="submit" name="add_category" class="btn btn-primary" value="Submit">
                  </form>
                </div>
                <!--/CARD BODY-->
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/ui/1.8.0/jquery-ui.js" integrity="sha256-d/98Bs8aAQdn5s3reV2fU8guAodmF4UhPjEt/pVkiJo=" crossorigin="anonymous"></script>
@endsection
