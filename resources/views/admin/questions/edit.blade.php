@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card shadow mb-4">
          <div class="card-header">
          <!-- CARD HEADER -->
            <h6 class="m-0 font-weight-bold text-primary">
                <i class="fa fa-plus"></i>Add Question
            </h6>
          </div>
          <!-- End of card header -->
          <!-- Card body -->
          <div class="card-body">
            <form class="kt-form" action={{ route('chapter.questions.update',[$chapter->id,$question->id]) }} method="POST" id="questionForm">
            @csrf
            @method('PUT')
                <div class="form-group">
                    <label>Enter The Question</label>
                    <input type="text" name="question" class="form-control {{ $errors->has('question') ? 'is-invalid' : '' }}" aria-describedby="emailHelp" placeholder="Enter the question" value="{{$question->question}}">
                    @error('question')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="d-flex">
                <div class="form-group col-md-6">
                    <label for="exampleSelect1">Select Marks</label>
                        <div></div>
                        <select class="custom-select form-control {{ $errors->has('marks') ? 'is-invalid' : '' }}" id="marks" name="marks">
                            <option disabled>Select</option>
                        <option value="1 " {{($question->marks ==  1 ? 'selected' : '')}}>1</option>
                            <option value="2 " {{($question->marks == 2 ? 'selected' : '')}}>2</option>
                        </select>
                        @error('marks')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="exampleSelect1">Select difficult Level</label>
                    <select class="custom-select form-control {{ $errors->has('marks') ? 'is-invalid' : '' }}" id="difficulty_lvl" name="difficulty_lvl">
                        <option disabled>Select</option>
                        <option value="1" {{($question->difficulty_level ==  1 ? 'selected' : '')}}>Easy</option>
                        <option value="2" {{($question->difficulty_level ==  2 ? 'selected' : '')}}>Moderate</option>
                        <option value="3" {{($question->difficulty_level ==  3 ? 'selected' : '')}}>Difficult</option>
                        <option value="4" {{($question->difficulty_level ==  4 ? 'selected' : '')}}>Toughest</option>
                    </select>
                    @error('difficulty_lvl')
                    <div class="text-danger">{{ $message }}</div>
                @enderror
                </div>
            </div>
                <div class="d-flex justify-content-between">
                    <h3>Add Answers</h3>
                    <a onclick="addOption()" class="add-opt btn btn-outline-primary">Add Option</a>
                </div>
            <div id="answer" class="pt-4">
                @php
                    $i=1;
                    $flag = 0;
                @endphp
                @foreach ($question->answers as $answer)
                <div class="form-group">
                    <label>Option {{$i}}</label>
                    <div class="d-flex justify-content-between">
                        <input type="text" name="answers[]" class="input form-control col-md-11" id="option_{{$i}}" aria-describedby="emailHelp" placeholder="Enter answer" value="{{$answer->answer}}">
                        <label class="kt-checkbox kt-checkbox--success justify-content-center">
                            @foreach($question->correct_answers as $correct_answer)
                                @if($correct_answer->id === $answer->id)
                                    <input type="checkbox"  class="cb" name="checkbox[]" id="checkbox_{{$i}}" data-id="{{$i}}" value="{{$answer->answer}}" checked>
                                    <span></span>
                                   @php
                                        $flag = 1;
                                   @endphp
                                @endif

                            @endforeach
                            @if ($flag==0)
                        <input type="checkbox" class="cb" name="checkbox[]" id="checkbox_{{$i}}" data-id="{{$i}}" value="{{$answer->answer}}">
                                <span></span>
                            @endif
                            @php
                                $flag=0;
                            @endphp
                        </label>
                    </div>
                </div>
                @php
                    $i++;
                @endphp
                @endforeach
        </div>

        <div>
            <button type="submit" class="btn btn-outline-success">Submit</button>
        </div>
        </form>
    </div>
</div>
      </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.8.0/jquery-ui.js" integrity="sha256-d/98Bs8aAQdn5s3reV2fU8guAodmF4UhPjEt/pVkiJo=" crossorigin="anonymous"></script>
<script>
var id =3;
function addOption(e) {
    $('#answer').append(` <div class='form-group'><label>Option ${id}</label><div class="d-flex justify-content-between">
                        <input type="text" name="answers[]" class="input form-control col-md-11" id="option_${id}" aria-describedby="emailHelp" placeholder="Enter Answer">

                        <label class="kt-checkbox kt-checkbox--success justify-content-center">
                            <input type="checkbox" onchange="myFunction()" class="cb" name="checkbox[]" id="checkbox_${id}" data-id="${id}" value="">
                            <span></span>
                        </label>

                    </div>`);
    id++;
}
$("#answer").on("keyup",".input",function(){
    var id = $(this).attr('id').split("_")[1];
    $("#checkbox_"+id).prop("checked", false);
    $("#checkbox_"+id).val("");
});

$("#answer").on("change",".cb",function(){

    var check = $(this);
    if ($(this).is(":checked"))
    {
        var element = "#option_"+($(this).data("id"));
        $(this).val($(element).val());
    }else{
        var element = "#option_"+($(this).data("id"));
        $(this).val("");
    }
});
$("#questionForm").validate({
    rules: {
            'checkbox[]': {required: true,minlength: 1}
    },
    messages: {
            "checkbox[]": "Please select at least one correct answer."
    }
});
</script>
@endsection
