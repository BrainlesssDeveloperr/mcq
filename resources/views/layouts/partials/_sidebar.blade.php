<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            @auth
                <ul class="kt-menu__nav ">
                    @can('isAdmin',auth()->user())
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="{{ route('dashboard') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-shelter"></i><span class="kt-menu__link-text">Dashboard</span>
                            </a>
                        </li>

                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                            <a href=" {{ route('subjects.index') }} " class="kt-menu__link"><i class="kt-menu__link-icon fa fa-newspaper"></i><span class="kt-menu__link-text">Subjects</span>
                            </a>
                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                </ul>
                            </div>
                        </li>
                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                            <a href=" {{ route('indexChapter') }} " class="kt-menu__link"><i class="kt-menu__link-icon fa fa-newspaper"></i><span class="kt-menu__link-text">Chapters</span>
                            </a>
                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                </ul>
                            </div>
                        </li>
                    @else
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="{{ route('dashboard') }}" class="kt-menu__link "><i class="kt-menu__link-icon flaticon2-shelter"></i><span class="kt-menu__link-text">Dashboard</span>
                            </a>
                        </li>

                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                            <a href=" {{ route('student.subjects.index') }} " class="kt-menu__link"><i class="kt-menu__link-icon fa fa-newspaper"></i><span class="kt-menu__link-text">Subjects</span>
                            </a>
                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                <ul class="kt-menu__subnav">
                                </ul>
                            </div>
                        </li>
                    @endcan

                </ul>
            @endauth
        </div>
    </div>

    <!-- end:: Aside Menu -->
</div>
