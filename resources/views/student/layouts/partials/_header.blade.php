<header class="header-area">
    <div class="header-top bg-img" style="background-image:url({{ asset('assets/student/img/icon-img/header-shape.png') }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7 col-12 col-sm-8">
                    <div class="header-contact">
                        <ul>
                            <li><i class="fa fa-phone"></i> +98 558 547 589</li>
                            <li><i class="fa fa-envelope-o"></i><a href="#">education@email.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5 col-12 col-sm-4">
                    <div class="login-register">
                        <ul>
                            <li><a href="login-register.html">Login</a></li>
                            <li><a href="login-register.html">Register</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom sticky-bar clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-4">
                    <div class="logo">
                        <a href="#">
                            <img alt="" src="{{ asset('assets/student/img/logo/logo.jpg') }}" class="logo-image">
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-6 col-8">
                    <div class="menu-cart-wrap">
                        <div class="main-menu">
                            <nav>
                                <ul>
                                    @include("student.layouts.partials._menu")
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
            <div class="mobile-menu-area">
                <div class="mobile-menu">
                    <nav id="mobile-menu-active">
                        <ul class="menu-overflow">
                            @include("student.layouts.partials._menu")
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
