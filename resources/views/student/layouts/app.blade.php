<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    @yield('meta')

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}">
   <!-- CSS
   ============================================ -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/student/css/bootstrap.min.css') }}">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{ asset('assets/student/css/icons.min.css') }}">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('assets/student/css/plugins.css') }}">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/student/css/style.css') }}">
    <!--begin::Page Vendors Styles(used by this page) -->

    <!--end::Global Theme Styles -->

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!--begin::Layout Skins(used by all pages) -->


    @yield('styles')
    <!-- Modernizer JS -->
    <script src="{{ asset('assets/student/js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>

<body>
<div class="book_preload">
    <div class="book">
        <div class="book__page"></div>
        <div class="book__page"></div>
        <div class="book__page"></div>
    </div>
</div>
@include('student.layouts.partials._header')

@yield('breadcrumb')

@yield('content')

@include('student.layouts.partials._footer')

<!-- JS
============================================ -->

<!-- jQuery JS -->
<script src="{{ asset('assets/student/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!-- Popper JS -->
<script src="{{ asset('assets/student/js/popper.min.js') }}"></script>
<!-- Bootstrap JS -->
<script src="{{ asset('assets/student/js/bootstrap.min.js') }}"></script>
<!-- Plugins JS -->
<script src="{{ asset('assets/student/js/plugins.js') }}"></script>
<!-- Ajax Mail -->
<script src="{{ asset('assets/student/js/ajax-mail.js') }}"></script>
<!-- Main JS -->
<script src="{{ asset('assets/student/js/main.js') }}"></script>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#22b9ff",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--end::Page Scripts -->
@yield('scripts')
</body>
</html>
