
@extends('layouts.dashboard')

@section('content')
<div class="wrapper d-flex flex-column mt-4">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow mb-4">
                    <div class="card-header">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <i class="fa fa-plus mr-2"></i>Create Practice Test
                        </h6>
                    </div>
                    <!--CARD BODY-->
                    <div class="card-body">
                        <h2 class="text-muted" style="margin-bottom: 20px!important">Chapters</h2>
                        <form action="{{route('subject.test.store',$subject->id)}}" method="POST" id="add-test">
                            @csrf
                            @php
                                $i=1;
                            @endphp
                            @foreach ($subject->chapters as $chapter)
                                <div class="form-group">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <label >{{$chapter->name}}</label>
                                        </div>

                                        <div class="col-md-1">
                                            <label class="kt-checkbox kt-checkbox--success justify-content-center">
                                                <input type="checkbox"
                                                        class="cb"
                                                        name="chapterSelected[]"
                                                        id="checkbox_{{$i}}" value="{{$chapter->id}}">

                                                <span></span>
                                            </label>
                                        </div>
                                        @php
                                            $maxMarks = $chapter->questions->sum('marks');
                                        @endphp

                                        <div class="col-md-2" id="keys">
                                            <input type="hidden" name="mainWeightage[]" id="w_{{$i}}">
                                            <input type="number"
                                                    name="weightage[]"
                                                    id="weightage_{{$i}}" value=""
                                                    placeholder="Weightage" class="d-inline form-control weightage {{ $errors->has('weightage') ? 'is-invalid' : '' }}"
                                                    disabled min="0" max="{{$maxMarks}}">
                                                    @error('weightage')
                                                         <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                        </div>
                                    </div>

                                </div>
                                @php
                                    $i++;
                                @endphp
                            @endforeach
                            <button type="submit" class="btn btn-outline-success">Create Test</button>
                        </form>
                    </div>
                    <!--/CARD BODY-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script>
        $(".cb").change(function(){

            var weightage = "#weightage_"+$(this).attr('id').split('_')[1];
            var w = "#w_"+$(this).attr('id').split('_')[1];
            var check = $(this);
            if ($(this).is(":checked"))
            {
                $(weightage).removeAttr('disabled');
            }
            else{
                $(weightage).val('');
                $(w).val('');
                $(weightage).attr('disabled',true);
            }
        });

        $('.weightage').keyup(function(){
            var weightage = $(this);
            var w = "#w_"+$(this).attr('id').split('_')[1];
            var check = "#checkbox_"+$(this).attr('id').split('_')[1];
            $(w).val($(check).val()+":"+$(weightage).val());

        });

    </script>
@endsection
