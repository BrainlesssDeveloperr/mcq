@extends('layouts.dashboard')
@section('breadcrumb')
    <div class="breadcrumb-area">
        <div class="breadcrumb-top default-overlay bg-img breadcrumb-overly-3 pt-100 pb-95" style="background-image:url({{ asset('assets/student/img/bg/breadcrumb-bg-5.jpg') }});">
            <div class="container">
                <h2>Quiz</h2>
            </div>
        </div>
        <div class="breadcrumb-bottom">
            <div class="container">
                <ul>
                    <li>
                        <a href="#">Home</a>
                        <span class="ml-2">
                            <i class="fa fa-angle-double-right"></i>
                            Quiz
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <section class="quiz-view">
        <div class="container">
            <div class="quiz-title">
                <h2>{{$test->subjects[0]->name}} {{$test->test_type}} Test</h2>

            </div>
            <div class="row">

                <div class="col-sm-8 col-md-9">
                    <div class="quiz-intro">
                        <h3>Instruction</h3>
                        <div class="form-group form-group-last">
                            <div class="alert pl-0" role="alert">
                                <div class="alert-icon pl-1"><i class="flaticon-warning kt-font-brand"></i></div>
                                <div class="alert-text">
                                    Refreshing the page, Changing tabs will <code>END THE TEST</code>
                                </div>
                            </div>
                        </div>
                        <div class="start-btn">
                            <a href="{{ route('tests.startTest',[$subject,$test]) }}" class="btn btn-lg btn-primary">Start Quizz</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
