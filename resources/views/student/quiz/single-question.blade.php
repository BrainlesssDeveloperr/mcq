{{-- {{dd(session()->all())}} --}}
@extends('layouts.dashboard')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/student/css/jquery.countdown.css') }}">
@endsection

@section('content')
    <section class="quiz-view">
        <div class="container">
            <div class="quiz-title">
                <h2>General Quiz</h2>
            </div>
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <div id="countdown"></div>
                    <div class="qustion-list" id="question-list">

                        @php
                            $activeArray = explode(":",session()->get('whichNoArray'));
                        @endphp
                        <div id="links">
                            @if (((int)session()->get('totalQuestions'))<=10)
                                @for($i = 1 ; $i<=((int)session()->get('totalQuestions')) ; $i++)
                                    <div data-id={{$i}} class="{{$i==1?'active':(in_array($i,$activeArray) ? 'fill' : '')}} qustion-slide">
                                        <div class="qustion-number">Q.{{$i}}</div>
                                    </div>
                                @endfor
                        </div>
                            @else
                                @for($i = 1 ; $i<=10 ; $i++)
                                    <div data-id={{$i}} class="{{$i==1?'active':(in_array($i,$activeArray) ? 'fill' : '')}} qustion-slide">
                                        <div class="qustion-number">Q.{{$i}}</div>
                                    </div>
                                @endfor
                        </div>
                                <div onclick="paginateQuestion(-1)" class="btn btn-info d-none mt-4" id="prevPage">Prev</div>
                                <div onclick="paginateQuestion(1)" class="btn btn-primary mt-4" id="nextPage">Next</div>
                            @endif
                        <input type="hidden" name="page-count" data-id="1" id="page-count">
                        <input type="hidden" name="" id="totalPageCount" data-id="{{session()->get('totalQuestionPage')}}">
                        <input type="hidden" name="" id="totalQuestions" data-id="{{session()->get('totalQuestions')}}">

                    </div>
                </div>
                <div class="col-sm-8 col-md-9">
                    <div class="qustion-main">
                        <div class="qustion-box">
                            <div  id="question-box">
                                <div id="question-data" data-id="1" class="qustion">{{$question->question}}</div>
                                <input type="hidden" value="{{$question->id}}" id="question">
                                <div class="ans">
                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($question->answers as $answer)
                                        <div class="ans-slide">
                                            @if($ans!=null)
                                                <label for="radio-{{$i}}"><input name="ans" id="radio-{{$i}}" value="{{$answer->id}}" {{$ans->id==$answer->id?'checked':''}} type="radio">{{$answer->answer}}</label>
                                            @else
                                                <label for="radio-{{$i}}"><input name="ans" id="radio-{{$i}}" value="{{$answer->id}}" type="radio">{{$answer->answer}}</label>
                                            @endif
                                        </div>

                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                </div>
                            </div>
                            <div class="save-btn">
                                <a href="#!" class="btn btn-sm btn-success" id="save">Save Ans</a>
                                <a href="#!" class="btn btn-sm btn-warning float-right" id="uncheckRadio">Uncheck all</a>
                            </div>
                            <div class="btn-slide">
                                <a href="#!" class="btn btn-sm btn-info mr-10" id="prev" >Prev Quest </a>
                                <a href="#" class="btn btn-sm btn-primary" id="next">Next Quest</a>
                                <a href="#!" class="btn btn-sm btn-outline-success d-none" id="next-unmarked">Next Unmarked Question</a>
                            </div>
                            <!-- Button trigger modal -->
                            <button style="border:none" type="button" class="" id="show-unmarked" data-toggle="modal" data-target="#unsaved"></button>
                        </div>
                        <div class="submit-quiz">
                            <a href="{{ route('quiz.result',$test->id) }}" onclick="return confirm('Are you sure you want to submit?');" class="btn btn-danger btn-lg">SUBMIT QUIZ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="unsaved" tabindex="-1" role="dialog" aria-labelledby="unsavedLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="unsavedLabel">Show Unmarked Questions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Do you want to visit the unmarked questions?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" id="yes-show-unmarked" class="btn btn-primary" data-dismiss="modal">Yes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/student/js/jquery.countdown.js') }}"></script>
    <script src="{{asset('assets/student/js/countdown.js')}}"></script>
    <script>
        var counter = null;
        var counter_stop = null;
        var time = 0;

        setTime({{session()->get('countdown')}});
        function setTime(x){
            time = x;
        }

        function preventPrevious(){
            var id = $('#question-data').data('id');
            // alert(id+" hieeee");
            if(id == 1){
                $('#prev').addClass("d-none").removeClass('d-inline-block');
            }
            else{
                $('#prev').removeClass("d-none").addClass('d-inline-block');
            }
        }

        function preventNext(){
            var id = $('#question-data').data('id');
            if(id == {{session()->get('totalQuestions')}}){
                $('#next').addClass("d-none");
            }
            else{
                $('#next').removeClass("d-none");
            }
        }


        function timer(t){
            var ts = new Date(2012, 0, 1),
                newYear = true;

            if((new Date()) > ts){
                // The new year is here! Count towards something else.
                // Notice the *1000 at the end - time must be in milliseconds
                // alert((new Date()).getTime());
                ts = {{session()->get('current_ts')* 1000}} + t ;//10*24*60*60*1000;

                newYear = false;
            }

            counter_stop = $('#countdown_stop').countdown({
                timestamp	: ts,
                callback	: function(days, hours, minutes, seconds){

                },
                stopeed : true
            });

            counter = $('#countdown').countdown({
                timestamp	: ts,
                callback	: function(days, hours, minutes, seconds){
                    if((new Date()).getTime() >= ts)
                    {
                        window.location.replace("{{ route('quiz.result',$test->id) }}");
                        alert("time is stoped");
                        window.YOUR_VARIABLE = setTimeout(tick, 1000);
                        clearTimeout(window.YOUR_VARIABLE);
                    }
                },
            });
        };
        $(function(){
            timer({{session()->get('countdown')}});
            preventPrevious();
            preventNext();
        });uncheckRadio
        $('#uncheckRadio').click(function(e){
            e.preventDefault();
            for(var i=1;i<=4;i++){
                var name = '#radio-'+i;
                $(name).prop('checked', false);

            }
        });
        $('#next-unmarked').click(function(e){
            e.preventDefault();
            getUnmarkedQuestion($("#question-data").data('id'),{{$subject->id}},{{$test->id}},1);
        });
        $('#yes-show-unmarked').click(function(e){
            e.preventDefault();
            getUnmarkedQuestion($("#question-data").data('id'),{{$subject->id}},{{$test->id}},-1);
        });
        $('#save').click(function(e){
            e.preventDefault();
            getDataSaved($("#question-data").data('id'),{{$subject->id}},{{$test->id}},0);
            if($("#question-data").data('id') == {{session()->get('totalQuestions')}}){
                $('#show-unmarked').click();
            }
        });
        $('#prev').click(function(e){
            e.preventDefault();
            getDataSaved($("#question-data").data('id'),{{$subject->id}},{{$test->id}},1);

        });
        $('#next').click(function(e){
            e.preventDefault();
            getDataSaved($("#question-data").data('id'),{{$subject->id}},{{$test->id}},0);

        });
        $('#question-list').on('click','.qustion-slide',function(){
            getQuestionFromSrNo($(this).data('id'),{{$subject->id}},{{$test->id}});
            /**
            *send this id in ajax the you will get the session for this
            *in that session split it with ':' then you will get question id on
            *first index and answer id on last index get them both and send back
            *by encoding jason and make the id active as well
            */
        });
        function getUnmarkedQuestion($srNo,$subject,$test,$n){
            var subjectID = $subject;
            var testID = $test;
            var active = $srNo;
            var n = $n;
            var route = "{{ route('ajax.unmarked') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'subjectID': subjectID,
                    'testID': testID,
                    'active': active,
                    'n':n
                },
                dataType: 'json',
                success: function(success){
                    if(success[0]=='done'){
                        $('#next-unmarked').removeClass('d-inline').addClass('d-none');
                    }else{
                        setQuestion(success);
                        setLinks(success);
                        $('#next-unmarked').removeClass('d-none').addClass('d-inline');
                    }
                }
            });
        }
        function setLinks(success){
            var active = success[2];
            var link ="";
            var arr = success[3].split(':');
            if(active.toString().length>1){
                if(success[2]%10==0){
                    var start =  success[2]-9;
                }else{
                    var start = success[2]-((success[2]%10)-1);
                }
            }else{
                var start = success[2]-(success[2]-1);
            }
            if(start%10==0)
                start = start+1;
            var end = start+9;
            if(end>{{session()->get('totalQuestions')}})
                end={{session()->get('totalQuestions')}};
            for(i=start;i<=end;i++){
                link += `<div data-id="${i}" class="qustion-slide ${active==i ? 'active' :(jQuery.inArray(i.toString(),arr)>=0?'fill':'' )}  "><div class="qustion-number">Q.${i}</div></div>`;
            }
            $("#links").html(link);
            if(active>=1 && active<=10){
                $("#nextPage").removeClass("d-none").addClass('d-inline-block');
                $("#prevPage").removeClass("d-inline-block").addClass('d-none');
            }else if(active<={{session()->get('totalQuestions')}} && active>=({{session()->get('totalQuestions')}}-({{session()->get('totalQuestions')}}%10))+1){
                $("#prevPage").removeClass("d-none").addClass('d-inline-block');
                $("#nextPage").removeClass("d-inline-block").addClass('d-none');
            }else{
                $("#prevPage").removeClass("d-none").addClass('d-inline-block');
                $("#nextPage").removeClass("d-none").addClass('d-inline-block');
            }
        }
        function getQuestionFromSrNo($srNo,$subject,$test){
            var subjectID = $subject;
            var testID = $test;
            var active = $srNo;
            var route = "{{ route('ajax.navigateQuestion') }}";
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'subjectID': subjectID,
                    'testID': testID,
                    'active': active
                },
                dataType: 'json',
                success: function(success){
                    setQuestion(success);
                    setLinks(success);
                }
            });
        }
        function getDataSaved($srNo,$subject,$test,$prev) {
            // alert($srNo);
            var subjectID = $subject;
            var testID = $test;
            var route = "{{ route('ajax.sendAnswer') }}";
            var radioValue = $("input[name='ans']:checked").val();
            var prev = $prev
            // alert(radioValue);
            var active = $srNo;
            $.ajax({
                url: route,
                method: "POST",
                data:{
                    _token: "{{ csrf_token() }}",
                    'subjectID': subjectID,
                    'testID': testID,
                    "question_id": $('#question').val(),
                    "answer_id": radioValue,
                    'active': active,
                    'prev':prev
                },
                dataType: 'json',
                success: function(success){
                    // if((active%10)==0)
                    //     paginateQuestion(1,active+1);
                    // if((active%10)==1 && active!=1 && prev==1)
                    //     paginateQuestion(-1,active-1);
                    setQuestion(success);
                    setLinks(success);
                }
            });
        }
        function setQuestion(success){
            console.log(success);
            var i=1;
            var s =``;
            var arr = [];
            if(success[3].includes(':'))
                var arr = success[3].split(':');
            else
                var arr = success[3];
            $(success[1]).each(function(ans){
                s += `<div class="ans"><div class="ans-slide">
                    <label for="radio-${i}"><input name="ans" ${success[1][ans].id==success[4]?'checked':''} id="radio-${i}" value="${success[1][ans].id}" type="radio">${success[1][ans].answer}</label>
                </div>`;
                i++;
            });
            $("#question-box").html(`
                <div id="question-data" data-id=${success[2]} class="qustion">${success[0].question}</div>
                <input type="hidden" value="${success[0].id}" id="question">
                ${success[0].marks}<br>
                ${success[0].difficulty_level}<br>
                ${success[0].id}
                `+s+"</div>"
            );
            var question_list = "";

            // var pageCount = $("#page-count").data('id');


            // var newId = $("#page-count").data('id');
            // var toId = newId+10;
            // var finalId = 0;
            // console.log("toId: :" +toId);
            // if((Math.floor(toId/10)) == $("#totalPageCount").data('id')){
            //     finalId = (toId -  $("#totalQuestions").data('id'))-1;
            //     console.log("finalID" +finalId);
            // }
            // var link ="";
            // for(i=newId;i<toId-finalId;i++){
            //     console.log(i);
            //     question_list += `<div data-id="${i}" class="qustion-slide ${success[2]==i ? 'active' :(jQuery.inArray(i.toString(),arr)>=0?'fill':'' )} "><div class="qustion-number">Q.${i}</div></div>`;
            // }

            // $("#links").html(question_list);

            preventPrevious();
            preventNext();

        }
        function preloadFunc()
        {
            // alert("{{((session()->get('current_ts')+7200000) - (now()->timestamp))}}");
            {{session()->put('countdown',((session()->get('current_ts')+session()->get('time')) - (now()->timestamp)))}}
        }
        window.onpaint = preloadFunc();

    </script>
    <script>
        window.onload = function () {
            document.onkeydown = function (e) {
                return (e.which || e.keyCode) != 116;
            };
        }
    </script>
    <script>
        $(document).on("keydown", function(e) {
            e = e || window.event;
            if (e.ctrlKey) {
                var c = e.which || e.keyCode;
                if (c == 82) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        });
    </script>

    <script>
        var no;
        function paginateQuestion(no,active=0){
            if(no==1){
                var id = $("#page-count").data('id');

                $("#page-count").data('id',id+10);
                var newId = $("#page-count").data('id');
                var toId = newId+10;
                var finalId = 0;
                console.log("toId: :" +toId);
                $("#prevPage").removeClass("d-none").addClass('d-inline-block');
                if((Math.floor(toId/10)) == $("#totalPageCount").data('id')){
                    $("#nextPage").addClass("d-none").removeClass('d-inline-block');
                    finalId = (toId -  $("#totalQuestions").data('id'))-1;
                    console.log("finalID" +finalId);
                }
                var link ="";
                var route = "{{ route('ajax.navigateFilledQuestion') }}";
                var arr= [];
                $.ajax({
                    url: route,
                    method: "POST",
                    dataType: 'json',
                    data:{
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(success){
                        console.log("sucesss : "+success);
                        arr = success.split(':');
                        for(i=newId;i<toId-finalId;i++){
                            console.log(i);
                            console.log(arr);
                            console.log(jQuery.inArray(i.toString(),arr)>=0);
                            link += `<div data-id="${i}" class="qustion-slide ${active==i ? 'active' :(jQuery.inArray(i.toString(),arr)>=0?'fill':'' )} "><div class="qustion-number">Q.${i}</div></div>`;
                        }
                        $("#links").html(link);
                    }

                });

            }else{
                var id = $("#page-count").data('id');

                $("#page-count").data('id',id-10);
                var newId = $("#page-count").data('id');
                var toId = newId+10;
                var finalId = 0;
                console.log("toId: :" +toId);
                $("#nextPage").removeClass("d-none").addClass('d-inline-block');
                if(newId==1){
                    $("#prevPage").addClass('d-none').removeClass('d-inline-block');
                }
                var link ="";
                var route = "{{ route('ajax.navigateFilledQuestion') }}";
                var arr= [];
                $.ajax({
                    url: route,
                    method: "POST",
                    dataType: 'json',
                    data:{
                        _token: "{{ csrf_token() }}",
                    },
                    success: function(success){
                        console.log("sucesss : "+success);
                        arr = success.split(':');
                        for(i=newId;i<toId-finalId;i++){
                            console.log(i);
                            console.log(arr);
                            console.log(jQuery.inArray(i.toString(),arr)>=0);
                            link += `<div data-id="${i}" class="qustion-slide ${active==i ? 'active' :(jQuery.inArray(i.toString(),arr)>=0?'fill':'' )}  "><div class="qustion-number">Q.${i}</div></div>`;
                        }
                        $("#links").html(link);
                    }

                });
            }
        }
    </script>
    <script>
        //submit on page reload
        if (performance.navigation.type == performance.navigation.TYPE_RELOAD || performance.navigation.type == performance.navigation.TYPE_BACK_FORWARD) {
            window.location.replace("{{ route('quiz.result',$test->id) }}");
        }
    </script>
    <script>
        window.addEventListener('blur', function sayHello(){
            window.location.replace("{{ route('quiz.result',$test->id) }}");
        });
    </script>
@endsection
