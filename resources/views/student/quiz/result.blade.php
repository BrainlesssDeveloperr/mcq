
@extends('layouts.dashboard')

@section('content')
    <section class="quiz-view">
        <div class="container">
            @if ($final_ans->isEmpty())
                <p class="text-success">Your test has been successfully submitted. All the Best for your results</p>
            @endif
            @if(!$final_ans->isEmpty())
                <div class="result d-flex justify-content-between">
                    <h3 class="title">Result:</h3>
                    <h3>You scored:<strong> {{$marks.'/'.$totMarks}}</strong></h3>
                </div>
                @foreach ($final_ans as $item)
                    <div class="col-md-12 pt-2">
                        <div class="qustion-main">
                            <div class="qustion-box">
                                <div  id="question-box">
                                    <div class="d-flex justify-content-between qustion">
                                        <div id="question-data" data-id="1" class="">{{$item[0]->question}}</div>
                                        <div id="marks" class="{{$item[1]->id == $item[2]->id ? 'correct':'wrong'}}">{{$item[0]->marks}} Marks</div>
                                    </div>
                                    <input type="hidden" value="" id="question">
                                    <div class="ans">
                                        @if ($item[1]->id == $item[2]->id)
                                            <div class="ans-slide">
                                                <p class="myAnswer m-0">Your answer:</p>
                                                <span class="correct">{{$item[1]->answer}}</span>
                                            </div>
                                        @else
                                            <div class="ans-slide">
                                                <p class="myAnswer m-0">Your answer:</p>
                                                <span class="wrong">{{$item[1]->answer}}</span>
                                            </div>
                                            <div class="ans-slide">
                                                <p class="myAnswer m-0">Correct answer:</p>
                                                <span class="correct">{{$item[2]->answer}}</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            </div>
    </section>
@endsection
@section('scripts')
<script type="text/javascript">
    function preventBack() {
        window.history.forward();
    }

    setTimeout("preventBack()", 0);

    window.onunload = function () { null };
</script>
@endsection



