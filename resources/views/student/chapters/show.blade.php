@extends('layouts.dashboard')

@section('content')
    <section id="scheduled-test">
        <div>
            <div class="card text-center">
                <div class="card-body">
                    <h3>
                        Scheduled Test:
                        @if ($testTime == null || $now>$endTime || $final_test==null)
                            No upcoming tests
                        @elseif($now>=$testTime && $now<$endTime)
                            <a href="{{route('student.test.index',[$subject->id,$final_test->id])}}">{{$subject->name}} Test</a>
                        @elseif($now<$testTime)
                            <strong> {{$testTime->diffForHumans().' : '.$testTime}}</strong>
                        @endif
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section class="quiz-view">
        <div class="container">
            <div class="quiz-title d-flex justify-content-between p-0">
                <h2 class="m-0">Chapters</h2>
            <a href="{{route('subject.test.create',$subject->id)}}" class="btn btn-outline-primary">Practice Test</a>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="qustion-list">
                        @foreach ($chapters as $chapter)
                            <div class="qustion-slide active">
                                <div class="qustion-number ">{{$chapter->name}}</div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    {{-- PREVIOUS TEST RESULTS --}}
        <div class="wrapper d-flex flex-column mt-2">
            <div class="container">
                <div id="kt_wrapper">
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                    <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <div class="kt-portlet kt-portlet--mobile">
                                <div class="kt-portlet__head kt-portlet__head--lg d-flex justify-content-between mt-2">
                                    <div class="kt-portlet__head-label">
                                        <span class="kt-portlet__head-icon">
                                            <i class="kt-font-brand flaticon2-line-chart"></i>
                                        </span>
                                        <h3 class="kt-portlet__head-title mt-2">
                                            Previous Tests
                                        </h3>
                                    </div>
                                    <div class="kt-portlet__head-toolbar">
                                        <div class="kt-portlet__head-wrapper">
                                            <div class="kt-portlet__head-actions">
                                                <div class="dropdown dropdown-inline">
                                                    <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="la la-download"></i> Export
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <ul class="kt-nav">
                                                            <li class="kt-nav__section kt-nav__section--first">
                                                                <span class="kt-nav__section-text">Choose an option</span>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-print"></i>
                                                                    <span class="kt-nav__link-text">Print</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-copy"></i>
                                                                    <span class="kt-nav__link-text">Copy</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                                    <span class="kt-nav__link-text">Excel</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-text-o"></i>
                                                                    <span class="kt-nav__link-text">CSV</span>
                                                                </a>
                                                            </li>
                                                            <li class="kt-nav__item">
                                                                <a href="#" class="kt-nav__link">
                                                                    <i class="kt-nav__link-icon la la-file-pdf-o"></i>
                                                                    <span class="kt-nav__link-text">PDF</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                &nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">

                                    <!--begin: Datatable -->
                                    <table class="table table-striped- table-bordered table-hover table-checkable" id="subject_table">
                                        <thead>
                                            <tr>
                                                <th>Chapter(s)</th>
                                                <th>Marks Obtained</th>
                                                <th>Out of</th>
                                                <th>Test Type</th>
                                                <th>Performed on</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             {{-- @foreach ($chapters as $chapter)
                                                 <tr>
                                                    <td>{{$chapter->id}}</td>
                                                    <td>{{$chapter->name}}</td>
                                                    <td></td>
                                                </tr>
                                            @endforeach --}}
                                        </tbody>
                                    </table>

                                    <!--end: Datatable -->
                                </div>
                            </div>
                        </div>

                        <!-- end:: Content -->
                    </div>
                </div>
            </div>

            <!-- end:: Content -->
        </div>
        {{-- </div> --}}
    </section>
@endsection
