
@extends('layouts.dashboard')
@section('title','Subjects')
@section('content')
    <div class="wrapper d-flex flex-column mt-4">
        <div class="container-fluid">
            <div id="kt_wrapper">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <!-- begin:: Content -->
                    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                        <div class="row">
                            @foreach($arr as $item)
                                <div class="col-xl-3 col-lg-6 order-lg-1 order-xl-1">

                                    <!--begin:: Widgets/Blog-->
                                    <div class="kt-portlet kt-portlet--height-fluid kt-widget19">
                                        <div class="kt-portlet__body kt-portlet__body--fit kt-portlet__body--unfill">
                                            <div class="kt-widget19__pic kt-portlet-fit--top kt-portlet-fit--sides" style="min-height: 300px; background-image: url(assets/admin/media//products/product4.jpg)">
                                                <h3 class="kt-widget19__title kt-font-light">
                                                    {{$item[0]->name}}
                                                </h3>
                                                <div class="kt-widget19__shadow"></div>
                                                <div class="kt-widget19__labels">
                                                    <a href="#" class="btn btn-label-light-o2 btn-bold btn-pill">{{$item[0]->chapters->count()}} Chapters</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget19__wrapper m-0">
                                                <p class="scheduled-test"><i class="fa fa-book"></i> Scheduled Test Link:
                                                    @if ($item[2] == null || $item[3]>$item[4] || $item[5]==null)
                                                        No upcoming test
                                                    @elseif($item[3]>=$item[2] && $item[3]<$item[4])
                                                        <a href="{{route('student.test.index',[$item[0]->id,$item[5]->id])}}">{{$item[0]->name}} Test</a>
                                                    @elseif($item[3]<$item[2])
                                                        <strong> {{$item[2]->diffForHumans().' : '.$item[2]}}</strong>
                                                    @endif
                                                </p>
                                                @if($item[5]!=null)
                                                    <p class="duration m-0"><i class="fa fa-clock"></i> Duration : {{gmdate("H", $item[5]->duration/1000)}} hours</p>
                                                @endif
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="kt-widget19__action d-flex">
                                                    <a href="{{route('subjects.chapters.show',$item[0]->id)}}" class="btn btn-sm btn-outline-success"><i class='kt-menu__link-icon flaticon-eye'></i>View Chapters</a>
                                                </div>
                                                <div class="kt-widget19__action d-flex">
                                                    <a href="{{route('subject.test.create',$item[0]->id)}}" class="btn btn-sm btn-outline-info"><i class='kt-menu__link-icon fa fa-book'></i>Practice Test</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end:: Widgets/Blog-->
                                </div>
                            @endforeach
                        </div>
                        <div class="d-flex justify-content-center">
                            {{$subjects->links()}}</div>

                    </div>
                    <!-- end:: Content -->
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('scripts')
    <script>
        alert("{{$subjects[2]->id}}");
    </script>
@endsection--}}

