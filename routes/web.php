<?php

use App\Http\Controllers\ResultsController;
use Illuminate\Support\Facades\Route;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

/**
 * ADMIN ROUTES
 */
Route::middleware(['verifyAdmin'])->group(function(){

    Route::get('/cpanel/dashboard', 'admin\DashboardController@index')->name('dashboard');
    Route::get('/cpanel/users', 'admin\UsersController@index')->name('users.index');


// Route::get('/cpanel/subject.chapter','admin\ChaptersController')->except(['index','show']);

    //new changes
    Route::get('/cpanel/chapters/create', 'admin\ChaptersController@createChapter')->name('createChapter');
    Route::get('/cpanel/chapters', 'admin\ChaptersController@indexChapter')->name('indexChapter');
    //end of new changes

    Route::resource('/cpanel/subject.chapters','admin\ChaptersController')->except(['edit','update','destroy']);
    Route::get('/cpanel/chapter/{chapter}/edit','admin\ChaptersController@editChapter')->name('chapter.editChapter');
    Route::put('/cpanel/chapter/{chapter}/update','admin\ChaptersController@updateChapter')->name('chapter.updateChapter');
    Route::delete('/cpanel/chapter/{chapter}/delete','admin\ChaptersController@destroyChapter')->name('chapter.destroyChapter');

    /**
     * QUESTIONS
     */
    Route::resource('/cpanel/chapter.questions', 'admin\QuestionsController');
    Route::get('/cpanel/chapters/{chapter}/questions/create','admin\QuestionsController@createQuestion')->name('questions.createQuestion');
    Route::get('/cpanel/chapters/{chapter}/questions/{question}/edit','admin\QuestionsController@editQuestion')->name('questions.editQuestion');
    /**
     * END OF QUESTIONS
     */

    /**
     * SUBJECTS
     */
    Route::resource('/cpanel/subjects','admin\SubjectsController');

    Route::resource('/cpanel/subjects.tests','admin\TestsController')->except(['edit','update','destroy']);
    /**
     * END OF SUBJECTS
     */

    Route::get('/cpanel/quiz/{test}/result','admin\ResultsController@getResult')->name('test.result');

});

/**
 * STUDENT ROUTES
 */
Route::middleware(['auth','verifyStudent'])->group(function(){

    Route::get('/student/dashboard', function(){
        return view('layouts.dashboard');
    })->name('dashboard');

    /**
     * SUBJECT
     */
    Route::resource('/student/subjects', 'student\SubjectsController', [
        'names' => [
            'index' => 'student.subjects.index',
        ]
    ]);
    /**
     * END OF SUBJECT
     */
    /**
     * CHAPTERS
     */
    Route::get('/student/subjects/{subject}/chapters','student\SubjectsController@show')->name('subjects.chapters.show');
     /**
      * END OF CHAPTERS
      */
    /**
     * QUIZ
     */
    Route::resource('/student/subject.test', 'student\TestsController');
    Route::get('/student/quiz', function (){

    })->name('quiz.index');
    Route::get('/student/quiz/single-question', function (){
        return view('student.quiz.single-question');
    })->name('quiz.question');

    Route::get('/student/subject/{subject}/test/{test}','student\TestsController@ready')->name('student.test.index');

    Route::get('/student/quiz/{test}/result','ResultsController@calculateResult')->name('quiz.result');

    Route::get('/subject/{subject}/test/{test}','student\TestsController@startTest')->name('tests.startTest')->middleware('isTestGiven');

    Route::post('ajax/ans','student\TestsController@processQuestion')->name('ajax.sendAnswer');
    Route::post('ajax/navigateQuestion','student\TestsController@navigateQuestion')->name('ajax.navigateQuestion');

    Route::post('ajax/navigate','student\TestsController@navigateFilledQuestion')->name('ajax.navigateFilledQuestion');

    Route::post('ajax/unmarked','student\TestsController@getUnmarkedQuestion')->name('ajax.unmarked');

    /**
      * END OF QUIZ
      */
});

